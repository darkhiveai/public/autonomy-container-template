#!/bin/bash

# TODO: This script is fairly brittle. It can fail if the registry container is stull running, 
# if host isn't on VPN, etc.

set -e # exit on error to prevent deploying bad/old image in case of error

# TODO: proper argument handling, with a --help
TARGET_IP=$1

# get host IP to be used for registry (assumes you are on zt vpn network, hence the 'ztn')
# TODO: infer the interface based on the TARGET_IP
INTERFACE=$(ip addr show | grep -B2 'ztn' | grep '^[0-9]' | awk '{print $2}' | sed 's/://')
HOST_IP=$(ip -o -4 addr list $INTERFACE | awk '{print $4}' | cut -d/ -f1)
echo "using HOST_IP = $HOST_IP"

# start a local docker registry to host the image
REGISTRY_NAME="dh_registry"
sudo docker run --rm -d -p 5000:5000 --name ${REGISTRY_NAME} registry:2
echo "started local registry: ${REGISTRY_NAME}"

# create & use cross-compilation builder if it doesn't already exist
BUILDER_NAME="dh_builder"
if ! sudo docker buildx ls | grep $BUILDER_NAME &> /dev/null; then
  sudo docker buildx create --name ${BUILDER_NAME} --use --bootstrap --driver-opt=network=host
fi

# build the runtime image and push to local registry
IMAGE_NAME="darkhive-autonomy-template"
sudo docker buildx build --push --platform linux/amd64,linux/arm64 --target runtime --tag localhost:5000/${IMAGE_NAME}:latest -f .devcontainer/Dockerfile .

# TODO: consider whether this is desired
# pull & tag image on host
# sudo docker pull localhost:5000/${IMAGE_NAME}
# sudo docker tag localhost:5000/${IMAGE_NAME} ${IMAGE_NAME}:latest
# echo "host pulled ${IMAGE_NAME} image from local registry"

# pull & tag image on target, note this is just for vanilla voxl hardware
sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@$TARGET_IP "docker pull ${HOST_IP}:5000/${IMAGE_NAME}"
sshpass -p oelinux123 ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@$TARGET_IP "docker tag ${HOST_IP}:5000/${IMAGE_NAME} ${IMAGE_NAME}:latest"
echo "target @ ${TARGET_IP} pulled ${IMAGE_NAME} image from local registry"

# stop the local registry
sudo docker container stop ${REGISTRY_NAME}
echo "stopped local registry: ${REGISTRY_NAME}"