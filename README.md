# Autonomy-Container-Template
This repository contains a base template for deploying autonomy code and pulls in supporting dependencies. We follow the [gitflow branching strategy](https://nvie.com/posts/a-successful-git-branching-model/), which means that all development work occurs on feature branches which are merged into the `develop` branch, which is in turn released (periodically, after QA) to `main`. Developers will typically want to pull `develop` with the `--recursive` option to also pull submodules:

`git clone --branch develop git@gitlab.com:darkhiveai/darkhive-private/autonomy/darkhive-autonomy.git --recursive`

The devcontainer in this repository allows developers build & deploy a component of the autonomy flight stack for SITL, HITL, and actual flight:

- PX4 firmware
- Containerized ROS2 environment
  - MicroDDS agent to bridge ROS2 <-> PX4
  

## Starting the devcontainer in VSCode

In order to build the devcontainer image and reopen the project in the container, install the `ms-vscode-remote.remote-containers` and `ms-vscode-remote.remote-ssh` extensions in VSCode. Then open the command palette (`ctrl-shift-p`) and search & execute for `Dev Containers: Rebuild and Reopen in Container`. Initially, this process will take quite some time as it builds the development docker image for the first time.

You can view the build log by clicking the `Starting Dev Container (show log)` link that appears in the bottom right of the window.

Additions to the development environment (e.g., apt installed dependecies) can be made in the `.devcontainer/Dockerfile`. Changes to the docker configuration (e.g., network, volume mounts, etc) can be made in the `.devcontainer/devcontainer.json` file. VSCode customization can also be made in the `devcontainer.json` file.


### Building for the Host
TODO: instructions for compiling/building your software to on the host computer (e.g., `make px4_sitl`)



## Darkhive Autonomy Docker Image
The `autonomy-container-template` docker image is a lightweight runtime image that is built on a ROS2 base image. Additionally, it has various dependencies installed to support the autonomy stack.


### Deploying Image to Target 
To deploy the image, a bash script (`deploy_autonomy_image.sh`) is provided. This script sets up a local docker registry, builds & pushes a multi-architecture image to the the registry, then pulls that image from the registry to the target platform over the Zero Tier Network.

```
./deploy_autonomy_image.sh <TARGET_IP>
```

### Testing on Target
To test the image on the target, you can run the following command to start a container on the target:


```
docker run --rm -it --net=host autonomy-container-template bash
```

```
ros2 topic list
```

**NOTE: to list topics you may need to `source /opt/ros/iron/setup.bash` as there appears to be an issue with the docker entrypoint for the moment.**

## FUTURE FUNCTIONALITY