#!/bin/bash

##EXAMPLES for sourcing... setting up for runtime etc. ####
# Source ROS 2
# source /opt/ros/${ROS_DISTRO}/setup.bash

# source the darkhive autonomy workspace
 
# Execute the command passed into this entrypoint
exec "$@"